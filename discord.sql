CREATE TABLE [Quotes] (
	[rawid] INTEGER PRIMARY KEY AUTOINCREMENT,
	[identifier] TEXT NOT NULL,
	[guildid] TEXT NOT NULL,
	[message] TEXT NOT NULL,
	[creatorid] TEXT NOT NULL
);

CREATE TABLE [Role_Messages] (
	[rawid] INTEGER PRIMARY KEY AUTOINCREMENT,
	[guildid] TEXT NOT NULL,
	[channelid] TEXT NOT NULL,
	[messageid] TEXT NOT NULL,
	[creatorid] TEXT NOT NULL,
	[stage] TEXT NOT NULL,
	[i] INTEGER NOT NULL,
	[emojiMap] BLOB NOT NULL
);
