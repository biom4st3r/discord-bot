import discord,requests
from discord.iterators import HistoryIterator
import json, random
import os, sys
import asyncio
import aiofile as aiof
from typing import List, Set, Dict, Tuple, Optional
import time, threading
import sqlite3
import atexit

db : sqlite3.Connection = sqlite3.connect('discord.db')

client = discord.Client()

def __end__():
    db.commit()
    db.close()

atexit.register(__end__)

def isGod(userid : int):
    return userid == 299255914916085761

permitted_users = [
    299255914916085761, # Curtis
    339030433381613568  # Ally
    ]

_embed : discord.Embed = discord.Embed()
_embed.colour = 0xf6db35

class Quote:
    def __init__(self):
        if os.path.exists('quotes.json'):
            with open('quotes.json') as file:
                self.quotes = json.loads(file.read())
        else:
            self.quotes = {}

        self.branch = {
            'a':self.add,
            'r':self.remove,
            's':self.set_value}
        pass
    async def parse(self, text : str, msg : discord.Message):
        text = text.split(' ')[1:]
        try:
            await self.branch[text[0][0]](text[1:],msg)
            await msg.add_reaction('👍')
        except KeyError:
            if isInt(text[0]):
                embed : discord.Embed = _embed.copy()
                if text[0] in self.quotes:
                    embed.description = self.quotes[text[0]] 
                else:
                    embed.description = 'There doesn\'t seem to be a quote there :grey_question:'
                await msg.channel.send(embed=embed)
                pass
            else:
                await msg.channel.send('Sorry I don\'t know what you mean :pensive:')
                pass
            pass
    async def add(self, text : list, msg : discord.Message):
        index = 0
        if len(self.quotes) != 0:
            index = self.quotes.keys()[-1]+1
        self.quotes[index] = ' '.join(text)
        await self.save()
    async def remove(self, text : list, msg : discord.Message):
        try:
            self.quotes.pop(text[0])
            await self.save()
        except:
            pass
    async def set_value(self, text : list, msg : discord.Message):
        self.quotes[text[0]] = ' '.join(text[1:])
        await self.save()
    async def save(self):
        async with aiof.async_open('quotes.json','w') as file:
            await file.write(json.dumps(self.quotes))

quote = Quote()

# embed.set_thumbnail(r'https://i.imgur.com/Y0OJpI6_d.webp?maxwidth=520&shape=thumb&fidelity=high')

class Enum:
    def __init__(self,name : str):
        self.name : str =  name
    def __eq__(self, value):
        return isinstance(value,Enum) and value.name == self.name

class States:
    DONE = Enum("done")
    WAIT_FOR_RESPONSE = Enum('wfr')
    @staticmethod
    def get(name : str):
        if name == 'done':
            return States.DONE
        elif name == 'wfr':
            return States.WAIT_FOR_RESPONSE

class ReactionPost:
    def __init__(self,msg : discord.Message,creator : int):
        self.creator : int = creator
        self.guild : int = msg.guild.id
        self.channel : int = msg.channel.id
        self._message : int = msg.id
        self.msg : discord.Message = msg
        self.stage : Enum = States.WAIT_FOR_RESPONSE
        self.index : int = 1
        self.emojiMap = {}
    def asJsonable(self) -> tuple:
        return (self.guild,self.channel,self._message,self.stage.name,self.index,self.emojiMap,self.creator)
    def fromJsonable(self,o : tuple):
        self.guild = o[0]
        self.channel = o[1]
        self._message = o[2]
        self.stage = States.get(o[3])
        self.index = o[4]
        self.emojiMap = o[5]
        self.creator = o[6]
        self.msg = client.get_guild(self.guild).get_channel(self.channel).get_partial_message(self._message)

# reaction_posts : Dict[int,ReactionPost] = {}
# with open('reaction_posts.json','r+') as file:
#     temp = json.loads(file.read())
#     for k,v in temp.items():
#         post = ReactionPost(None,None)
#         post.fromJsonable(v)
#         reaction_posts[k] = post
#     pass

async def create_role_selector(message : discord.Message):
    if message.guild:
        guild : discord.Guild = message.guild
        index = 1 # skip built in

        await message.channel.send('Simply react with the Emoji you want assosiated with the role. or X if you want to skip the role')
        msg : discord.Message = await message.channel.send(guild.roles[index].name)
        await msg.add_reaction('❌')
        reaction_posts[msg.id] = ReactionPost(msg,message.author.id)
        

async def update_role_selection(reaction : discord.Reaction, user : discord.User):
    if client.user == user:
        return
    entries = db.execute('SELECT channelid,messageid,creatorid,stage,i,emojimap FROM Role_Messages WHERE guildid = ?',(reaction.message.guild.id,)).fetchall()
    entries : List[Tuple[int,int,int,str,int,Any]] = list(map(lambda a: list(a),entries))
    for entry in entries:
        if entry[1] == reaction.message.id: 
            pass
    
# async def update_role_selection(reaction : discord.Reaction, user : discord.User):
#     if client.user == user:
#         return
#     if reaction.message.id in reaction_posts:
#         post : ReactionPost = reaction_posts[reaction.message.id]
#         if user.id != post.creator:
#             await reaction.remove(user)
#             return
#         if post.stage is States.WAIT_FOR_RESPONSE:
#             pass
#             if reaction.emoji == '❌':
#                 pass
#             else:
#                 post.emojiMap[reaction.emoji] = reaction.message.guild.roles[post.index].id
#             post.index+=1
#             if post.index >= len(post.msg.guild.roles): # Finished
#                 post.stage = States.DONE
#                 embed : discord.Embed = _embed.copy()
#                 embed.title = 'Please select the roles you want!'
#                 embed.description = ''
#                 for k,v in post.emojiMap.items():
#                     embed.description+= k + ': ' + reaction.message.guild.get_role(v).name + '\n'
#                 msg : discord.Message = await reaction.message.channel.send(embed=embed)
#                 for k in post.emojiMap.keys():
#                     await msg.add_reaction(k)
#                 await post.msg.delete()
#                 reaction_posts.pop(post._message)
#                 post.msg = msg
#                 post._message = msg.id
#                 reaction_posts[post._message] = post
#                 return
#             await post.msg.edit(content=post.msg.guild.roles[post.index].name)
#             await post.msg.clear_reactions()
#             await post.msg.add_reaction('❌')
#             serial = {}
#             for k,v in reaction_posts.items():
#                 pass
#                 serial[k] = v.asJsonable()
#             async with aiof.async_open('reaction_posts.json','w+') as file:
#                 await file.write(json.dumps(serial))
#         elif post.stage is States.DONE:
#             guild : discord.Guild = post.msg.guild
#             if reaction.emoji in post.emojiMap:
#                 member : discord.Member = guild.get_member(user.id)
#                 await member.add_roles(guild.get_role(post.emojiMap[reaction.emoji]))
#     else: # Ignore
#         pass
#     pass

async def ping(message : discord.Message):
    if random.randint(0,49) == 0:
        await message.channel.send('ping')
        await message.channel.send('No...wait...')
    await message.channel.send('pong')

permitted_clear_channels = []

async def permit_clear(message : discord.Message):
    channel : discord.TextChannel =  message.channel
    permitted_clear_channels.append(channel.id)
    await message.add_reaction('👍')

async def clearChannel(message : discord.Message):
    if message.channel.id in permitted_clear_channels:
        permitted_clear_channels.remove(message.channel.id)
        channel : discord.TextChannel = message.channel
        history : HistoryIterator = channel.history(limit=100)
        await channel.delete_messages(await history.flatten())
    else:
        await message.channel.send('Clearing is not permitted in this channel. Please use ##permit_clear first')

async def list_commands(message : discord.Message):
    await message.channel.send('My commands are:\n%s' % list(commands.keys()))
    pass

def isInt(val):
    try:
        int(val)
        return True
    except:
        return False

class Idable:
    def __init__(self,id):
        self.id = id

async def submit_quote(message : discord.Message):
    text : List[str] = message.content.split(' ')
    text = text[1:]
    action = text[0] if len(text) != 0 else None
    identifier = text[1] if len(text) > 1 else None
    content = ' '.join(text[2:]) if len(text) > 2 else None
    _rawid : List[Tuple[Any]] = db.execute(f'SELECT rawid FROM Quotes WHERE (guildid = {str(message.guild.id)}) AND (identifier = ?)',(identifier if identifier else action,)).fetchall()
    rawid : int = _rawid[0][0] if len(_rawid) > 0 else None
    if not action:
        await message.channel.send(f'Syntax ##Quote [-add,-remove,-set,-author,id] [quote_name] [content...]')
    elif action == '-add': #add
        if rawid:
            await message.channel.send(f'Sorry {identifier} is already taken. Try `quote s`')
        else:
            db.execute('INSERT INTO Quotes (identifier,guildid,message,creatorid) VALUES(?,?,?,?)',(identifier, message.guild.id, content, message.author.id))
            db.commit()
            await message.add_reaction('👍')
    elif action == '-remove': #remove
        if rawid:
            db.execute(f'DELETE FROM Quotes WHERE rawid = {rawid}')
            db.commit()
            await message.add_reaction('👍')
        else:
            await message.add_reaction('❓')
    elif action == '-set': #set
        if rawid:
            creator = db.execute(f'SELECT creatorid FROM Quotes WHERE rawid = {rawid}').fetchall()[0][0]
            if message.author.id == isGod(message.author.id):
                db.execute(f'UPDATE Quotes SET message = ? WHERE (rawid = {rawid})',(content,))
            else:
                await message.channel.send(f'Sorry that quote doesn\'t belong to you. You can ask {await message.guild.fetch_member(creator).name} to change it!')
        else:
            db.execute('INSERT INTO Quotes (identifier,guildid,message,creatorid) VALUES(?,?,?,?)',
                (identifier, message.guild.id, content, message.author.id))
        db.commit()
        await message.add_reaction('👍')
        pass
    elif action == '-author':
        creator = db.execute(f'SELECT creatorid FROM Quotes WHERE rawid = {rawid}').fetchall()[0][0]
        await message.channel.send(f'{await message.guild.fetch_member(creator)}')
    elif action == '-list':
        quotes = db.execute(f'SELECT identifier FROM Quotes WHERE guildid = {message.guild.id}').fetchall()
        embed : discord.Embed = _embed.copy()
        mapped = list(map(lambda a: a[0],quotes))
        embed.description = ''
        for x in mapped:
            embed.description += x 
            embed.description += ', '
        embed.description = embed.description[:-2]
        embed.title = "List of Quotes"
        await message.channel.send(embed=embed)
        pass
    else:
        if rawid:
            embed : discord.Embed = _embed.copy()
            embed.title = f"Quote #{action}"
            embed.description = db.execute(f'SELECT message FROM Quotes WHERE rawid = {rawid}').fetchall()[0][0]
            await message.channel.send(embed = embed)
        else:
            await message.channel.send(f'Sorry I can\'t find `{action}`')
    pass

async def logout(message : discord.Message):
    if not isGod(message.author.id):
        await message.channel.send('Only Biom4st3r can use this command.')
        return
    await message.add_reaction('💤')
    await client.logout()

def permit_all(user : discord.User)-> bool:
    return True
def permit_god(user : discord.User) -> bool:
    return isGod(user.id)
def permit_ally(user : discord.User) -> bool:
    return isGod(user.id) or user.id == 339030433381613568

commands = {
    'ping':(lambda u: True,ping),
    'permit_clear':(permit_ally,permit_clear),
    'clear':(permit_ally,clearChannel),
    'list_commands':(permit_all,list_commands),
    'quote':(permit_all,submit_quote),
    'create_role_selector':(permit_god,create_role_selector),
    'go_to_sleep':(permit_god,logout),
    '!':(permit_all,submit_quote)
}

async def tryCommand(text ,message : discord.Message) -> bool:
    if(len(text) > 0):
        try:
            if(commands[text[0]][0](message.author)):
                await commands[text[0]][1](message)
                return True
        except KeyError:
            print('Invalid Command!')
            await message.add_reaction('🙅')
            return True
    return False


@client.event
async def on_message(message : discord.Message):
    if message.author is client:
        return
    if str(message.content).startswith('##'):
        if await tryCommand(message.content[2:].split(' '),message):
            return
        else:
            await message.channel.send('You\'re not allowed to use that command :angry:')
        pass
    author : discord.User = message.author
    if author.id == 339030433381613568 and random.randint(0,99) == 0: # Ally
        await message.add_reaction('❤️')
    return

@client.event
async def on_reaction_add(reaction : discord.Reaction, user : discord.User):
    # await update_role_selection(reaction,user)
    pass

@client.event
async def on_ready():
    print('Started')

client.run(os.getenv('BIOBOT'))